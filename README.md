
#Search exercise


##Introduction


You have been given the task to add features to a prototype for people searching which was originally built as a proof of concept.

##Current functionality

* The system has the concept of a customer. A customer is either a premium customer or a non paying customer.
* The system searches people data based on surname + postcode as search criteria
* The system return search results as a collection of records. Each record consist of:
    * Person details such as forename, middle name, surname, telephone
    * Address details such as build number, street, postcode and town
    * Indicator where the source data come from. A record can be sourced from 1 or many datasources (source types)
* The system return data from the following data sources (source types)
    * BT (British Telecom)
    * DNB (Duns & Bradstreet)
    * ELECTORAL_ROLL (UK Electoral Roll)
* There are currently no restrictions on searching or displaying results
* Searching is currently free

##New functionality

###Requirement 1

* Ensure that the system only return BT specific records for non paying customers.
In other words, non paying customers are only allowed to view records where BT is the exclusive data source.
* Premium customers can view data from all three data sources (BT, DNB, ELECTORAL_ROLL).

**Hint:** Treat the search engine as a black box. The focus of this task is to manipulate what comes back from the search engine

###Requirement 2

Premium customers have purchased 192 credits in advance which they intend to use. Your task is to implement a
mechanism to charge premium customers based on the data coming back from the search engine.
The system should charge premium customers 1 credit per record returned with the following exception.
We don't charge for individual records which exclusively contain data sourced from "BT"

**Hint:** A legacy charging service has been provided for you. Your task is to integrate with the
following service `net.icdpublishing.exercise2.myapp.charging.services.ChargingService`

###Requirement 3

To implement a simple authentication mechanism based on customers email addresses.
In other words, each search request needs to be authenticated against existing customer database.
If the customer cannot be found the system should return an appropriate exception.

**Hint:** The following interface have been provided for you to query existing
customer database `net.icdpublishing.exercise2.myapp.customers.dao.CustomerDao`

###Requirement 4

Order search results based on surname (ascending order)

##Technical details:
Environment:
The application you will be working on is a maven project.
Feel free to add any other dependencies you may need and the same goes for dependencies
for testing frameworks you might want to use. Feel free to use any testing framework you like.

Design:
The focus of this task is to demonstrate good software design and coding practices.
You are free to make assumptions as long as you state them in a separate readme file.
There is no UI to build for this prototype.